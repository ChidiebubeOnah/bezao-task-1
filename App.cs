﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAO___Task_1.Models;

namespace BEZAO___Task_1
{
    public class App
    {
        public static void Run()
        {
            Console.WriteLine("Hello, Welcome to the BEZAO Console App, My name is Task 1.\nMay I know Your name? please Enter your Name: ");

            string name = Console.ReadLine();

            while (String.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Opps, I couldn't get your name\nPlease try again!");
                name = Console.ReadLine();
            }

            Console.WriteLine($"Nice meeting you {name}!\nKnowing Your Birthday will let me know on how best to communicate with" +
                              $" you;\nEnter your Date of Birth in this format 'YYYY/MM/DD': ");

            string birthday = Console.ReadLine();

            while (String.IsNullOrWhiteSpace(birthday))
            {
                Console.WriteLine("Sorry, I couldn't get your birthday \nPlease try again!");
                birthday = Console.ReadLine();
            }


            DateTime dateOfBirth = DateTime.ParseExact(birthday, "yyyy/MM/dd", CultureInfo.InvariantCulture);




            while (dateOfBirth.Year > DateTime.Now.Year)
            {
                Console.WriteLine("you can't be born in the  future!!!\nEnter your Date of Birth in this format 'YYYY/MM/DD': ");
                birthday = Console.ReadLine();
            }



            User user = new User(name, dateOfBirth);


            Console.WriteLine($"My name is {user.GetUserName()} , and I am {user.GetAge()} years old.");
        }
    }
}
