﻿using System;

namespace BEZAO___Task_1.Models
{
    public class User
    {
        private string _name;
        private DateTime _dateOfBirth;

        public User(string name, DateTime dateOfBirth)
        {
            this._name = name;
            this._dateOfBirth = dateOfBirth;

        }


        public string GetUserName()
        {
            return this._name;
        }

        public int GetAge()
        {
            return DateTime.Now.Year - this._dateOfBirth.Year;
        }

    }
}