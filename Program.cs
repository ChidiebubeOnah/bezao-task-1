﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAO___Task_1.Models;

namespace BEZAO___Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
           
            try
            { 
                App.Run();
            }
            catch (System.FormatException e)
            {
                Console.WriteLine($"The Date Format Supplied is not supported!\n{e}");
                
            }
        }

    }
}
